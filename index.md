<html><head><meta charset='utf-8'><meta id="ForPhone" name="viewport" content="width=device-width, initial-scale=1.0"><title>Ma page Term bac pro</title> </head>

  

  
  
<body>
  
  
# Ressources Term bac pro

  <br>

<div> Dépôt contenant les ressources pour la classe de Terminale professionnelle.
  
  Pour retrouver les autres niveaux, [cliquez sur ce lien](https://tmounier.forge.aeif.fr/maths-sciences/).</div>

  <br>
  
<p><strong>Les ressources  </strong></p>
  

<div> Les documents sont disponibles, la plupart du temps, au format .tex (LaTeX) ou au format .pdf pour une utilisation direct en cas de besoin.
</div>

---
  
## Maths
  
0. [Des activités autour de Python et de l'algorithmie](https://forge.aeif.fr/tmounier/term-tex/-/tree/main/Maths/Python%20algo)  
1. [Fonctions](https://forge.aeif.fr/tmounier/term-tex/-/tree/main/Maths/Fonctions)  (Révisions + fonction dérivée)
2. [Statistiques à 2 variables](https://forge.aeif.fr/tmounier/term-tex/-/tree/main/Maths/Stats)  (Affine et non affine)


Et aussi des [activités diverses](https://forge.aeif.fr/tmounier/term-tex/-/tree/main/Maths/Maths%20divers).

## Physique - chimie


1. [Pression dans un fluide](https://forge.aeif.fr/tmounier/1ere-tex/-/tree/main/Sciences/Pression) 

## Le livret de mathématiques

Je mets à dispostion de mes élèves un [livret](https://forge.aeif.fr/tmounier/term-tex/-/blob/main/Maths/livret%20maths/livret%20term.pdf) contenant des exercices. 
  Ces exercices sont de l'application de cours, des prises d'initiative, des problèmes ouverts. 
  Ce document remplace (partiellement) un manuel et se veut un compagnon pour l'autonomie de l'élève, l'accompagnement personnalisé et l'approfondissement.
 
## Pour la classe
  
Dans ce [répertoire](https://forge.aeif.fr/tmounier/term-tex/-/tree/main/Divers) quelques activité inclassables (bilans, prof principal...)
  

## Des ressources .tex 
  
Afin d'ouvrir et de compiler les documents, j'ai aussi placé [à la racine du dépôt](https://forge.aeif.fr/tmounier/term-tex/-/tree/main/Packages) mes packages persos.


## Des ressources libres
  
  
En attente d'une plus profonde réflexion (et gestion des conflits), mes ressources sont distribuées sous licence 
  ![](https://forge.aeif.fr/tmounier/term-tex/-/raw/main/ccby.png)


  </body></html>