﻿def suite(raison,debut,nbterme):
    """Construit et affiche la suite arithmétique avec les paramètres donnés par l'utilisateur"""
    liste=[] #liste vide pour stocker les termes 
    for i in range(nbterme):
        liste.append(debut+i*raison)
    return(liste)
