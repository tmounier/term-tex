def suite(v,n,q):
    """Calcule et affiche termes jusqu'à u_n d'une suite géométrique de raison q"""
    """Retourne aussi la somme des n termes"""
    i=0
    s=0
    while i!=(n+1):
        s=s+v
        print(v)
        v=v*q
        i=i+1
        
    print("la somme est S=",s)