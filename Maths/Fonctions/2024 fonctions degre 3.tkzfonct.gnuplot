set table "2024 fonctions degre 3.tkzfonct.table"; set format "%.5f"
set samples 200.0; plot [x=0:6.000000000000000000] ((-0.9*(x*1)**3+8*(x*1)**2-24.4*(x*1)+20)-0)/5
