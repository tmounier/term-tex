set table "2024 evaluation polynome deg3.tkzfonct.table"; set format "%.5f"
set samples 200.0; plot [x=0:14.000000000000000000] ((0.21*(x*2)**3-7.2*(x*2)**2+46.9*(x*2)+40.5)-0)/50
